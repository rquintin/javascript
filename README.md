# ABOUT THE PROGRAM – A WALK THROUGH

1. The HTML file defines the text and formatting information for a simple web page. You can change anything you want to see how it affects the formatting of the page. This isn’t really programming, but it is fun to play with. It:

a. Creates a title for the website
b. Tells the HTML code to use Javascript and where to find the code
c. Formats the displayed text
d. Specifies a text entry box with ID = “MyInputTemp” (Javascript will need this)
e. Specifes a button labled “Go!”
f. When the mouse button is pressed, runs the javascript bePrepared() function
g. displays the string in “myAnswer” which is created in the javascript program.

2. The Javascript file has a single function called “bePrepared().” It is run anytime the GO button is clicked (look at the “onclick” method for the “input” = “Button” tag in the HTML file to see the function call.)

3. The “bePrepared()” function

a. Obtains the value entered in the “Input”-“Text” tag by using it’s ID: “MyInputTemp”
b. The value is converted to Celcius (with a single decimal place).
c. The Celsius value is evaluated with a seris of IF Statements and a suggested action phrase is assigned to the variable: “myActionText”
d. A sentence is created by combining or “concatenating” some static text along with the temperature values (deg F and Deg C) and the action phrase.
e. The new phrase is inserted onto the last line of theHTML page by using the ID of the tag on that line: “myAnswer”

# TRY THIS

1. Change the temperatures used in the decisions – change the lower temperature from 60 to 30 degrees, for example. Make sure you change it in two places! Save the file and refresh the browser (or restart the web page), and enter new numbers – did the answers change at the new temperature?

2. Create a new temperature range from 30 to 60 degrees and have it display – “Bring hat and gloves!”

3. Change the wording of the phrases.

4. Add another text input – ask for the wind speed, for example.

5. Add some conditional statements that evaluate the wind chill factor.

6. Add some text to display the wind chill result.

Credit: [Boys Life Magazine](https://boyslife.org/about-scouts/merit-badge-resources/programming/41261/javascript/)
